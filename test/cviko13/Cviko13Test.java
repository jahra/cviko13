/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cviko13;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Honza
 */
public class Cviko13Test {
    
    public Cviko13Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    

    @Test
    public void testRpnCalc() {
        System.out.println("rpnCalc");
        String expr = "1 1 + 3 - 2 * 2 /";
        double expResult = 1.0;
        double result = Cviko13.rpnCalc(expr);
        assertEquals(expResult, result, 0.0);                
    }

    @Test
    public void testIsNumeric() {
        System.out.println("isNumeric");
        String str = "10";
        boolean expResult = true;
        boolean result = Cviko13.isNumeric(str);
        assertEquals(expResult, result);
    }

    @Test
    public void testWriteTextMatrix() throws IOException {
        System.out.println("writeTextMatrix");
        Writer w = new FileWriter("writeMatrix.txt");
        int[][] matrix = {{1,2,3},{3,2,1},{1,1,1}};
        Cviko13.writeTextMatrix(w, matrix);
    }

    @Test
    public void testReadTextMatrix() throws Exception {
        System.out.println("readTextMatrix");
        Reader r = new FileReader("readMatrix.txt");
        int[][] expResult = {{1 , 2, 3, 4}, {2, 3, 4, 5}, {3,4,5,6},{5,6,7,8}};
        int[][] result = Cviko13.readTextMatrix(r);
        assertArrayEquals(expResult, result);
    }
    
}
