/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cviko13;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Honza
 */
public class Cviko13 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public static double rpnCalc(String expr)
    {
            String[] splittedExpr = expr.split(" ");
            Deque<Double> deq = new LinkedList<>();
            double x,y;
            
            for (int i = 0; i < splittedExpr.length; i++) {
                //when number
                if (isNumeric(splittedExpr[i]))
                {
                    deq.push(Double.valueOf(splittedExpr[i]));                    
                }else //2x pop 1x push
                {
                    if( splittedExpr[i].equals("+"))deq.push(deq.pop() + deq.pop());
                    if( splittedExpr[i].equals("-"))deq.push(deq.pop() - deq.pop());
                    if( splittedExpr[i].equals("*"))deq.push(deq.pop() * deq.pop());
                    if( splittedExpr[i].equals("/"))deq.push(deq.pop() / deq.pop());
                }
            }
            return deq.pop();
    }
    
    public static boolean isNumeric(String str)
    {
            for (char c : str.toCharArray())
            {
                if (!Character.isDigit(c)) return false;
            }
            return true;
    }
        
    public static void writeTextMatrix(Writer w, int[][] matrix){
        PrintWriter printWritter = new PrintWriter(w);
        for (int[] m1 : matrix) {
            for (int m2 : m1) {
                printWritter.print(m2 + " ");
            }
        printWritter.println();
        }
        printWritter.close();
    }
    
    public static int[][] readTextMatrix(Reader r) throws FileNotFoundException, IOException{
                
        int aX = 0, aY = 0;
        int[][] array = new int[aX][aY];
        int j = 1;
        Scanner sc = null;
        
        sc = new Scanner(new BufferedReader(r));
        
        String[] _line = null;
        if(sc.hasNextLine()){
            _line = sc.nextLine().split(" ");
            array = new int[1][_line.length];
            for (int i = 0; i < _line.length; i++) {
                array[0][i] = Integer.parseInt(_line[i]);
            }
        }
        while(sc.hasNextLine()){
            _line = sc.nextLine().split(" ");           
            array = enlargeArray(array);
            aX++;
            for (int i = 0; i < _line.length; i++) { 
                array[aX][i] = Integer.parseInt(_line[i]);
            }
        }
        return array;
    }
    
    private static int[][] enlargeArray(int[][] oldArray){
        int[][] newArray = new int[oldArray.length + 1][oldArray[0].length];
        for (int i = 0; i < oldArray.length; i++) {
            System.arraycopy(oldArray[i], 0, newArray[i], 0, oldArray[i].length);
        }
        return newArray;
    }
    
}
